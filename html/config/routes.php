<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/** Front Actions */
$app->get('/', function () use ($app, $request) {
    $controller = new App\Front\Controllers\Home($request);
    return $app['twig']->render('index.html.twig', $controller->indexAction());
})->bind('homepage');

/** Api Actions */
$app->get("/api/places/{latitude}/{longitude}", function ($latitude, $longitude) use ($app, $request) {
    $model = new \App\Api\Models\Places($app['places']);
    $controller = new App\Api\Controllers\Places($request, $app['response'], $model);
    $response = $controller->getAction($latitude, $longitude);
    return $response;
});

$app->get("/api/place/{idPlace}", function ($idPlace) use ($app, $request) {
    $model = new \App\Api\Models\Place($app['places']);
    $controller = new App\Api\Controllers\Place($request, $app['response'], $model);
    $response = $controller->getAction($idPlace);
    return $response;
});

/** only for development - clear cache storage */
$app->delete("/api/clear", function () use ($app, $request) {
    if (!$app['debug']) {
        throw new NotFoundHttpException('Debug is disable now');
    }
    $model = new \App\Api\Models\Places($app['places']);
    $controller = new App\Api\Controllers\Places($request, $app['response'], $model);
    $response = $controller->clearAction();
    return $response;
});


/** Errors */
$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }
    $msg = $e->getMessage();
    $model = new \App\Api\Models\Errors();
    $controller = new App\Api\Controllers\Errors($request, $app['response'], $model);
    $response = $controller->indexAction($msg, $code);

    return $response;
});
