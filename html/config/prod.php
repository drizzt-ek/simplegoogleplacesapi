<?php

use Silex\Provider\MonologServiceProvider;
use Silex\Provider\WebProfilerServiceProvider;
use ValueObjects\String\String;

$app['twig.path'] = array(__DIR__ . '/../templates');
$app['twig.options'] = array('cache' => __DIR__ . '/../var/cache/twig');

$app['debug'] = false;

$app['google.places.apikey'] = String::fromNative('AIzaSyBYmHWJQ9LwvEtR_CJM9NivuYHxSvIZn_0');

$app['response'] = function () use ($app) {
    return new \Symfony\Component\HttpFoundation\JsonResponse();
};
$app['places'] = function () use ($app) {
    return new \Infrastructure\Places\GooglePlaces($app['google.places.apikey']);
};

Infrastructure\Cache\Cache::init(new \Infrastructure\Storage\Redis());

if ($app['debug']) {
    $app->register(new MonologServiceProvider(), array(
        'monolog.logfile' => __DIR__ . '/../var/logs/silex_dev.log',
    ));

    $app->register(new WebProfilerServiceProvider(), array(
        'profiler.cache_dir' => __DIR__ . '/../var/cache/profiler',
    ));
}
