<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-05
 * Time: 15:17
 */

namespace Core;

$tpath = realpath(dirname(__FILE__) . '/../../');
require_once $tpath . '/vendor/autoload.php';
require_once $tpath . '/tests/Mock/ApiController.php';

use App\Api\Models\Errors;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiControllerTest extends \PHPUnit_Framework_TestCase
{

    /** @var ApiController */
    public static $instance;

    /**
     *
     */
    public static function setUpBeforeClass()
    {
        $model = new Errors();
        $response = new JsonResponse();
        $request = Request::createFromGlobals();
        self::$instance = new \Mock\ApiController($request, $response, $model);
    }

    /**
     * @param $data
     * @param $statusCode
     * @dataProvider getResponseDataProvider
     */
    public function testGetResponse($data, $statusCode)
    {
        /** @var JsonResponse $response */
        $response = self::$instance->getResponseE($data, $statusCode);

        if (!empty($data['statusCode'])) {
            $statusCode = $data['statusCode'];
        }
        $this->assertEquals($statusCode, $response->getStatusCode());
    }

    public function getResponseDataProvider()
    {
        return [
            [[], 204],
            [['statusCode'=>204], 204]
        ];
    }
}