<?php

namespace App\Api\Models;

$tpath = realpath(dirname(__FILE__) . '/../../../../');

require_once $tpath . '/tests/Mock/Places.php';
require_once $tpath . '/vendor/autoload.php';

use Infrastructure\Cache\Cache;
use Infrastructure\Cache\Place;
use Infrastructure\Storage\Redis;
use ValueObjects\Geography\Latitude;
use ValueObjects\Geography\Longitude;
use ValueObjects\String\String;

/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-04
 * Time: 14:36
 */
class PlacesTest extends \PHPUnit_Framework_TestCase
{
    public static function setUpBeforeClass()
    {
        $redis = new Redis();
        Cache::init($redis);
    }

    /**
     * @dataProvider placeDataProvider
     */
    public function testGetPlaces($lat, $lng, $helperData)
    {
        Place::del(String::fromNative('1'));
        Place::del(String::fromNative('2'));
        Place::del(String::fromNative('3'));
        $mock = new \Mock\Places();
        $mock->setResults($helperData);

        $place = new Places($mock);
        $result = $place->getPlaces($lat, $lng);
        $this->assertEquals($helperData, $result);
    }

    /**
     * @dataProvider placeDataProvider
     * @throws \Exception
     */
    public function testValidateParameters($lat, $lng, $helperData)
    {
        Place::del(String::fromNative('1'));
        Place::del(String::fromNative('2'));
        Place::del(String::fromNative('3'));
        $mock = new \Mock\Places();
        $mock->setResults($helperData);

        $place = new Places($mock);
        $result = $place->getPlaces($lat, $lng);
        $this->assertEquals($helperData, $result);
    }

    /**
     * @param $lat
     * @param $lng
     * @param $helperData
     * @dataProvider placesDataProvider
     */
    public function testCompleteListElementsFromCache($lat, $lng, $helperData, $expectedResult)
    {
        $mock = new \Mock\Places();
        $mock->setResults($helperData);

        Place::set(String::fromNative('1'), 'test');
        Place::set(String::fromNative('3'), 'test');

        $place = new Places($mock);
        $result = $place->completeListElementsFromCache($helperData, $lat, $lng);
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @return array
     */
    public function placesDataProvider()
    {
        return [
            [Latitude::fromNative(12.2), Longitude::fromNative(12.0), ['results' => [['place_id' => '1'], ['place_id' => '2'], ['place_id' => '3']]], ['results' => [['place_id' => '1', 'details' => 'test'], ['place_id' => '2'], ['place_id' => '3', 'details' => 'test']]]]
        ];
    }

    public function placeDataProvider()
    {
        return [
            [12.2, 12.0, ['statusCode' => 200, 'results' => [['place_id' => '1'], ['place_id' => '2'], ['place_id' => '3']]]]
        ];
    }

    public function errorDataProvider()
    {
        return [
            ['"<>?,../', 'd', ['test0', 'test1', 'test2']]
        ];
    }
}
