<?php

namespace App\Api\Models;

$tpath = realpath(dirname(__FILE__) . '/../../../../');

require_once $tpath . '/tests/Mock/Places.php';
require_once $tpath . '/vendor/autoload.php';

use Infrastructure\Cache\Cache;
use Infrastructure\Storage\Redis;
use Mock\Places;

/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-04
 * Time: 14:36
 */
class PlaceTest extends \PHPUnit_Framework_TestCase
{
    public static function setUpBeforeClass()
    {
        $redis = new Redis();
        Cache::init($redis);
    }

    /**
     * @dataProvider placeDataProvider
     */
    public function testGetPlace($idPlace, $helperData)
    {
        $mock = new Places();
        $mock->setResults($helperData);

        $place = new Place($mock);
        $result = $place->getPlace($idPlace);
        $this->assertEquals($helperData, $result);
    }

    /**
     * @dataProvider placeDataProvider
     * @throws \Exception
     */
    public function testValidateIdPlace($idPlace, $helperData)
    {
        $mock = new Places();
        $mock->setResults($helperData);

        $place = new Place($mock);
        $result = $place->getPlace($idPlace);
        $this->assertEquals($helperData, $result);
    }

    /**
     * @return array
     */
    public function placeDataProvider()
    {
        return [
            ['1234567890aA-_', ['statusCode' => 200, 'results' => ['test0', 'test1', 'test2']]]
        ];
    }

    public function errorDataProvider()
    {
        return [
            ['"<>?,../', ['statusCode' => 200, 'results' =>['test0', 'test1', 'test2']]]
        ];
    }
}
