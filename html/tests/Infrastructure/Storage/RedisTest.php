<?php

namespace Infrastructure\Storage;

use ValueObjects\String\String;

$tpath = realpath(dirname(__FILE__) . '/../../../');
require_once $tpath . '/vendor/autoload.php';

/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-04
 * Time: 14:36
 */
class RedisTest extends \PHPUnit_Framework_TestCase
{
    /** @var Redis */
    public static $redisInstance;

    public static function setUpBeforeClass()
    {
        self::$redisInstance = new Redis();
    }

    /**
     * @dataProvider gettersSettersDataProvider
     */
    public function testGettersAndSetters($key, $value)
    {
        self::$redisInstance->set($key, $value);
        $this->assertEquals($value, self::$redisInstance->get($key));
    }
    /**
     * @dataProvider gettersSettersDataProvider
     */
    public function testDelete($key, $value)
    {
        self::$redisInstance->set($key, $value);
        self::$redisInstance->del($key);
        $this->assertEquals(false, self::$redisInstance->get($key));
    }

    /**
     * @return array
     */
    public function gettersSettersDataProvider()
    {
        return [
            [String::fromNative('key'), 1],
            [String::fromNative('key'), 2]
        ];
    }
}
