<?php

namespace Infrastructure\Cache;

use Infrastructure\Storage\Redis;
use ValueObjects\Geography\Latitude;
use ValueObjects\Geography\Longitude;

$tpath = realpath(dirname(__FILE__) . '/../../../');
require_once $tpath . '/vendor/autoload.php';

/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-04
 * Time: 14:36
 */
class PlacesTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Predis\Client */
    public static $redisInstance;

    public static function setUpBeforeClass()
    {
        $storage = new Redis();
        Cache::init($storage);
    }

    /**
     * @dataProvider gettersSettersDataProvider
     */
    public function testGettersAndSetters($latitude, $longitude, $value)
    {
        Places::set($latitude, $longitude, $value);
        $this->assertEquals($value, Places::get($latitude, $longitude));
    }

    /**
     * @dataProvider gettersSettersDataProvider
     */
    public function testDelete($latitude, $longitude, $value)
    {
        Places::set($latitude, $longitude, $value);
        Places::del($latitude, $longitude);
        $this->assertEquals(false, Places::get($latitude, $longitude));
    }

    /**
     * @return array
     */
    public function gettersSettersDataProvider()
    {
        return [
            [Latitude::fromNative(12.2), Longitude::fromNative(12.0), 1],
            [Latitude::fromNative(12.2), Longitude::fromNative(12.0), 2]
        ];
    }
}
