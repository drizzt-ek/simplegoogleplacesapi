<?php

namespace Infrastructure\Cache;

use Infrastructure\Storage\Redis;
use ValueObjects\String\String;

$tpath = realpath(dirname(__FILE__) . '/../../../');
require_once $tpath . '/vendor/autoload.php';

/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-04
 * Time: 14:36
 */
class CacheTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Predis\Client */
    public static $redisInstance;

    public static function setUpBeforeClass()
    {
        $storage = new Redis();
        Cache::init($storage);
    }

    /**
     * @dataProvider gettersSettersDataProvider
     */
    public function testGettersAndSetters($key, $value)
    {
        Cache::set($key, $value);
        $this->assertEquals($value, Cache::get($key));
    }

    /**
     * @dataProvider gettersSettersDataProvider
     */
    public function testDelete($key, $value)
    {
        Cache::set($key, $value);
        Cache::del($key);
        $this->assertEquals(false, Cache::get($key));
    }

    /**
     * @return array
     */
    public function gettersSettersDataProvider()
    {
        return [
            [String::fromNative('key'), 1],
            [String::fromNative('key'), 2]
        ];
    }
}
