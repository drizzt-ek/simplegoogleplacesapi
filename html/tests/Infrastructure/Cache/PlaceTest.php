<?php

namespace Infrastructure\Cache;

use Infrastructure\Storage\Redis;
use ValueObjects\String\String;

$tpath = realpath(dirname(__FILE__) . '/../../../');
require_once $tpath . '/vendor/autoload.php';

/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-04
 * Time: 14:36
 */
class PlaceTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Predis\Client */
    public static $redisInstance;

    public static function setUpBeforeClass()
    {
        $storage = new Redis();
        Cache::init($storage);
    }

    /**
     * @dataProvider gettersSettersDataProvider
     */
    public function testGettersAndSetters($key, $value)
    {
        Place::set($key, $value);
        $this->assertEquals($value, Place::get($key));
    }

    /**
     * @dataProvider gettersSettersDataProvider
     */
    public function testDelete($key, $value)
    {
        Place::set($key, $value);
        Place::del($key);
        $this->assertEquals(false, Place::get($key));
    }

    /**
     * @return array
     */
    public function gettersSettersDataProvider()
    {
        return [
            [String::fromNative('key'), 1],
            [String::fromNative('key'), 2]
        ];
    }
}
