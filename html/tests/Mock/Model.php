<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-05
 * Time: 15:15
 */

namespace Mock;

$tpath = realpath(dirname(__FILE__) . '/../../');
require_once $tpath . '/vendor/autoload.php';

class Model implements \Core\Model
{

}