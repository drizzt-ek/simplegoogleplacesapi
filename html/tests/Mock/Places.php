<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-04
 * Time: 16:39
 */

namespace Mock;

$tpath = realpath(dirname(__FILE__) . '/../../');
require_once $tpath . '/vendor/autoload.php';

use ValueObjects\Geography\Latitude;
use ValueObjects\Geography\Longitude;
use ValueObjects\String;

class Places implements \Infrastructure\Places\Places
{
    /**
     * @var
     */
    public $results;

    /**
     * @param $results
     */
    public function setResults($results)
    {
        $this->results = $results;
    }

    /**
     * @param Latitude $latitude
     * @param Longitude $longitude
     * @return array
     */
    public function getBarsInRadiusFromLocation(Latitude $latitude, Longitude $longitude)
    {
        return $this->results;
    }

    /**
     * @param String\String $idPlace
     * @return array
     */
    public function getDetailsAboutPlace(String\String $idPlace)
    {
        return $this->results;
    }
}