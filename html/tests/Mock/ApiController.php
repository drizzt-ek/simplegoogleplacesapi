<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-05
 * Time: 15:23
 */

namespace Mock;

$tpath = realpath(dirname(__FILE__) . '/../../');
require_once $tpath . '/vendor/autoload.php';

class ApiController extends \Core\ApiController
{
    public function getResponseE($data, $statusCode)
    {
        return $this->getResponse($data, $statusCode);
    }
}