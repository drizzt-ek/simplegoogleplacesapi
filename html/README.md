About application
=================
This is a very simple application to cache results from requests to api Google Places. There are two layers of the front and api. The purpose of the first of these is polling second of data collected from GooglePlases. 
In Front layer was used:
-jQuery
-Bootstrap
In Api layer was used:
-Silex framework - because is silm
-Redis - to data persistance



How to run an application
=======================
1. Run the virtual machine. You should do this by Vagrant, by command in console **vagrant up** (use this command in "schibsted" folder)
2. add to etc/hosts line:
192.168.56.101       schibsted.dev
3. open http://schibsted.dev/ website 

WARNING: Geolocalization is not working on google chrome without a certificate SSL.
