<?php
//ini_set('display_errors', E_ALL);
ini_set('display_errors', 0);
require_once __DIR__ . '/../vendor/autoload.php';

$app = require __DIR__ . '/../src/app.php';
require __DIR__ . '/../config/prod.php';
require __DIR__ . '/../config/routes.php';
$app->run();
