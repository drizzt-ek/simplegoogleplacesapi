class Places {
    placesUrl:string;
    placeDetailUrl:string;

    constructor() {
        this.placesUrl = 'http://schibsted.dev/api/places/<lat>/<lng>';
        this.placeDetailUrl = 'http://schibsted.dev/api/place/<placeId>';
    }

    private _getPlacesUrl(latitude:string, longitude:string) {
        return this.placesUrl.replace("<lat>", latitude).replace("<lng>", longitude);
    }

    private _getPlaceDetailUrl(placeId:string) {
        return this.placeDetailUrl.replace("<placeId>", placeId);
    }

    public getPlaces(latitude:string, longitude:string) {
        let url = this._getPlacesUrl(latitude, longitude);
        let renderer = new Renderer();
        renderer.clearElementList();
        renderer.setLoaderToElementList();
        correctUrl=url;
        $.ajax({
            url: url,
            success: function (data, textStatus, xhr) {
                if (xhr.status == 200) {
                    let addedElementWithDetails :boolean;
                    $.each(data.results, function (index, object) {
                        addedElementWithDetails = renderer.replacePlaceByElementFromPlaces(object);
                        if (!addedElementWithDetails) {
                            setTimeout(function () {
                                let places = new Places();
                                places.getPlaceDetails(object,url);
                            }, 500);

                        }
                    });
                } else {
                    new ErrorManager().show(xhr.status);
                    renderer.addEmptyData();
                }
                console.log(xhr.status + "--" + new Date().getTime());
            },
            complete: function (xhr, textStatus) {
                console.log(xhr.status + "--" + new Date().getTime());
            }
        });
    }

    public getPlaceDetails(place,url) {
        let placeId = place.place_id;
        let requestUrl=url;
        $.ajax({
            url: this._getPlaceDetailUrl(placeId),
            success: function (placeData, textStatus, xhr) {
                let renderer = new Renderer();
                console.log(requestUrl);
                console.log(correctUrl);
                if( requestUrl==correctUrl){
                    if (xhr.status == 200 ) {
                        renderer.replacePlaceByDetailsElement(placeData.result);
                    } else {
                        new ErrorManager().show(xhr.status, place);
                    }
                }
            }
        });
    }
}
class Renderer {
    elementTemplate;
    loaderTemplate;

    constructor() {
        this.elementTemplate = $("#patternElement>tbody").html();
        this.loaderTemplate = $("#patternLoader").html();
    }

    public replacePlaceByElementFromPlaces(place) {
        let placeId = place.place_id;
        this._deleteOldElementIfExist(placeId);
        let element = this._setToDOM(this.elementTemplate);
        element.attr('id', placeId);

        let resultAddedElementWithDetails = false;
        if (place.details == undefined) {
            this._createElement(place, element);
        } else {
            if (place.details.statusCode != 200) {
                this._createElement(place, element);
            } else {
                this._createElementWithDetails(place.details.result, element);
                resultAddedElementWithDetails = true;
            }
        }
        return resultAddedElementWithDetails;
    }

    public replacePlaceByDetailsElement(place) {
        let placeId = place.place_id;
        this._deleteOldElementIfExist(placeId);
        let element = this._setToDOM(this.elementTemplate);
        element.attr('id', placeId);

        this._createElementWithDetails(place, element);

    }

    public addEmptyData(){
        let id = 'em';
        this.clearElementList();
        let element = this._setToDOM(this.elementTemplate);
        element.attr('id', id);
        this._createEmptyDataElement(element);
    }

    private _createEmptyDataElement( element) {
        element.find('.name').html('Nie znaleziono elementów do wyświetlenia');
    }

    private _createElement(place, element) {
        element.find('.icon').html(this.loaderTemplate);
    }

    private _createElementWithDetails(place, element) {
        let icon = '<img class="icon" src="<url>"/>'.replace("<url>", place.icon);
        element.find('.icon').html(icon);

        let name = place.name;
        element.find('.name').html(name);

        let address = place.adr_address + '<div class="details hide"><phone></div>'.replace('<phone>', place.formatted_phone_number);
        element.find('.address').html(address);

        let location = place.geometry.location.lat + "," + place.geometry.location.lng;
        location = '<a href="<url>" target="_blank"><loc></a>'.replace("<url>", place.url).replace('<loc>',location);
        location = location + '<div class="details hide"><div class="map"></div></div>';
        element.find('.location').html(location);
    }

    private _setToDOM(element) {
        $("#elements").prepend(element);
        return $("#elements").find('#place_id');
    }
    public setLoaderToElementList(){
        $("#elements").html(this.loaderTemplate+" Ładuję listę");
    }
    public clearElementList() {
        $("#elements").html('');
    }

    private _deleteOldElementIfExist(placeId:string) {
        let elementDOM = $('#' + placeId);
        if (elementDOM != undefined) {
            $('#' + placeId).remove();
        }
    }

    private _getTemplateElement() {
        return $("#place_id");
    }
}
class ErrorManager {
    public show(errorCode:string, place) {
        console.log("error:" + errorCode);
    }
}
let neptuneLatitude = "54.348548";
let neptuneLongitude = "18.653235";

$(function () {
    let correctUrl=null;
    let places = new Places();
    places.getPlaces(neptuneLatitude, neptuneLongitude);
    $("#search_your").click(function () {
        navigator.geolocation.getCurrentPosition(function(position) {
            places.getPlaces(position.coords.latitude, position.coords.longitude);
        },function(){
            alert('Coś poszło nie tak z pobraniem twojej lokalizacji');
        });
    });
    $("#search_neptune").click(function () {
        places.getPlaces(neptuneLatitude, neptuneLongitude);
    }
});
