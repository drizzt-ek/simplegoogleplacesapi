var Places = (function () {
    function Places() {
        this.placesUrl = 'http://schibsted.dev/api/places/<lat>/<lng>';
        this.placeDetailUrl = 'http://schibsted.dev/api/place/<placeId>';
    }

    Places.prototype._getPlacesUrl = function (latitude, longitude) {
        return this.placesUrl.replace("<lat>", latitude).replace("<lng>", longitude);
    };
    Places.prototype._getPlaceDetailUrl = function (placeId) {
        return this.placeDetailUrl.replace("<placeId>", placeId);
    };
    Places.prototype.getPlaces = function (latitude, longitude) {
        var url = this._getPlacesUrl(latitude, longitude);
        var renderer = new Renderer();
        renderer.clearElementList();
        renderer.setLoaderElementList();
        correctUrl = url;
        $.ajax({
            url: url,
            success: function (data, textStatus, xhr) {
                if (xhr.status == 200) {
                    var addedElementWithDetails_1;
                    $.each(data.results, function (index, object) {
                        addedElementWithDetails_1 = renderer.replacePlaceByElementFromPlaces(object);
                        if (!addedElementWithDetails_1) {
                            setTimeout(function () {
                                var places = new Places();
                                places.getPlaceDetails(object, url);
                            }, 500);
                        }
                    });
                }
                else {
                    new ErrorManager().show(xhr.status);
                    renderer.addEmptyData();
                }
            },
            complete: function (xhr, textStatus) {
                renderer.removeLoaderElementList();
            }
        });
    };
    Places.prototype.getPlaceDetails = function (place, url) {
        var placeId = place.place_id;
        var requestUrl = url;
        $.ajax({
            url: this._getPlaceDetailUrl(placeId),
            success: function (placeData, textStatus, xhr) {
                var renderer = new Renderer();
                console.log(requestUrl);
                console.log(correctUrl);
                if (requestUrl == correctUrl) {
                    if (xhr.status == 200) {
                        renderer.replacePlaceByDetailsElement(placeData.result);
                    }
                    else {
                        new ErrorManager().show(xhr.status, place);
                    }
                }
            }
        });
    };
    return Places;
}());
var Renderer = (function () {
    function Renderer() {
        this.elementTemplate = $("#patternElement>tbody").html();
        this.loaderTemplate = $("#patternLoader").html();
    }

    Renderer.prototype.replacePlaceByElementFromPlaces = function (place) {
        var placeId = place.place_id;
        this._deleteOldElementIfExist(placeId);
        var element = this._setToDOM(this.elementTemplate);
        element.attr('id', placeId);
        var resultAddedElementWithDetails = false;
        if (place.details == undefined) {
            this._createElement(place, element);
        }
        else {
            if (place.details.statusCode != 200) {
                this._createElement(place, element);
            }
            else {
                this._createElementWithDetails(place.details.result, element);
                resultAddedElementWithDetails = true;
            }
        }
        return resultAddedElementWithDetails;
    };
    Renderer.prototype.replacePlaceByDetailsElement = function (place) {
        var placeId = place.place_id;
        this._deleteOldElementIfExist(placeId);
        var element = this._setToDOM(this.elementTemplate);
        element.attr('id', placeId);
        this._createElementWithDetails(place, element);
    };
    Renderer.prototype.addEmptyData = function () {
        var id = 'em';
        this.clearElementList();
        var element = this._setToDOM(this.elementTemplate);
        element.attr('id', id);
        this._createEmptyDataElement(element);
    };
    Renderer.prototype._createEmptyDataElement = function (element) {
        element.find('.name').html('Nie znaleziono elementów do wyświetlenia');
    };
    Renderer.prototype._createElement = function (place, element) {
        element.find('.icon').html(this.loaderTemplate);
    };
    Renderer.prototype._createElementWithDetails = function (place, element) {
        var icon = '<img class="icon" src="<url>"/>'.replace("<url>", place.icon);
        element.find('.icon').html(icon);
        var name = place.name;
        element.find('.name').html(name);
        var address = place.adr_address + '<div class="details hide"><phone></div>'.replace('<phone>', place.formatted_phone_number);
        element.find('.address').html(address);
        var location = place.geometry.location.lat + "," + place.geometry.location.lng;
        location = '<a href="<url>" target="_blank"><loc></a>'.replace("<url>", place.url).replace('<loc>', location);
        location = location + '<div class="details hide"><div class="map"></div></div>';
        element.find('.location').html(location);
    };
    Renderer.prototype._setToDOM = function (element) {
        $("#elements").prepend(element);
        return $("#elements").find('#place_id');
    };
    Renderer.prototype.setLoaderElementList = function () {
        $("#elements").html('<tr id="loading_list"><td>' + this.loaderTemplate + " Loading<td><tr>");
    };
    Renderer.prototype.removeLoaderElementList = function () {
        $("#loading_list").remove();
    };
    Renderer.prototype.clearElementList = function () {
        $("#elements").html('');
    };
    Renderer.prototype._deleteOldElementIfExist = function (placeId) {
        var elementDOM = $('#' + placeId);
        if (elementDOM != undefined) {
            $('#' + placeId).remove();
        }
    };
    Renderer.prototype._getTemplateElement = function () {
        return $("#place_id");
    };
    return Renderer;
}());
var ErrorManager = (function () {
    function ErrorManager() {
    }

    ErrorManager.prototype.show = function (errorCode, place) {
        console.log("error:" + errorCode);
    };
    return ErrorManager;
}());
var neptuneLatitude = "54.348548";
var neptuneLongitude = "18.653235";
$(function () {
    var places = new Places();
    places.getPlaces(neptuneLatitude, neptuneLongitude);
    $("#search_your").click(function () {
        navigator.geolocation.getCurrentPosition(function (position) {
            places.getPlaces(position.coords.latitude, position.coords.longitude);
        }, function () {
            alert('Something went wrong with downloading your location');
        });
    });
    $("#search_neptune").click(function () {
        places.getPlaces(neptuneLatitude, neptuneLongitude);
    });
});
