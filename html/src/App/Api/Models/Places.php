<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-03
 * Time: 10:51
 */

namespace App\Api\Models;


use Core\Model;
use Infrastructure\Cache;
use Predis\Client;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use ValueObjects\Geography\Latitude;
use ValueObjects\Geography\Longitude;
use ValueObjects\String;

class Places implements Model
{
    /** @var \Infrastructure\Places\Places $_places */
    private $_places;

    /**
     * Places constructor.
     * @param \Infrastructure\Places\Places $places
     */
    public function __construct(\Infrastructure\Places\Places $places)
    {
        $this->_places = $places;
    }

    /**
     * @param $latitude
     * @param $longitude
     * @return mixed
     */
    public function getPlaces($latitude, $longitude)
    {
        try {
            $latitude = Latitude::fromNative($latitude);
            $longitude = Longitude::fromNative($longitude);
        } catch (\Exception $e) {
            throw new UnprocessableEntityHttpException();
        }

        $places = Cache\Places::get($latitude, $longitude);

        if (empty($places) || $places['statusCode'] != Response::HTTP_OK) {
            $places = $this->_places->getBarsInRadiusFromLocation($latitude, $longitude);
            Cache\Places::set($latitude, $longitude, $places);
        }
        $places = $this->completeListElementsFromCache($places, $latitude, $longitude);

        return $places;
    }

    /**
     * @param $places
     * @param Latitude $latitude
     * @param Longitude $longitude
     * @return mixed
     */
    public function completeListElementsFromCache($places, Latitude $latitude, Longitude $longitude)
    {
        if (empty($places['results'])) {
            return $places;
        }
        $changeList = false;
        foreach ($places['results'] as &$place) {
            $idPlace = String\String::fromNative($place['place_id']);
            if (empty($place['details']) && $placeDetails = Cache\Place::get($idPlace)) {
                $place['details'] = $placeDetails;
                $changeList = true;
            }
        }
        if ($changeList) {
            Cache\Places::set($latitude, $longitude, $places);
        }
        return $places;
    }

    /**
     * Method create only for dev
     * @return array
     */
    public function clear()
    {
        $redis = new Client();
        $keys = $redis->keys('*');
        foreach ($keys as $item) {
            $redis->del($item);
        }
        return $keys;
    }
}