<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-03
 * Time: 10:51
 */

namespace App\Api\Models;


use Core\Model;
use Infrastructure\Cache;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use ValueObjects\String;

class Place implements Model
{
    /** @var \Infrastructure\Places\Places $_places */
    private $_places;

    /**
     * Place constructor.
     * @param \Infrastructure\Places\Places $places
     */
    public function __construct(\Infrastructure\Places\Places $places)
    {
        $this->_places = $places;
    }

    /**
     * @param $idPlace
     * @return array
     */
    public function getPlace($idPlace)
    {
        $idPlace = preg_replace('/[^A-Za-z0-9_\-]/', '', $idPlace);
        if (empty($idPlace)) {
            throw new UnprocessableEntityHttpException();
        }
        $idPlace = String\String::fromNative($idPlace);

        $placeDetails = Cache\Place::get($idPlace);
        if (empty($placeDetails) || $placeDetails['statusCode'] != Response::HTTP_OK) {
            $placeDetails = $this->_places->getDetailsAboutPlace($idPlace);
            Cache\Place::set($idPlace, $placeDetails);
        }

        return $placeDetails;
    }
}