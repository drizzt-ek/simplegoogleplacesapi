<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-03
 * Time: 16:45
 */

namespace App\Api\Controllers;


use Core\ApiController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Errors
 * @package App\Api\Controllers
 */
class Errors extends ApiController
{
    /**
     * @param $msg
     * @param $code
     * @return Response
     */
    public function indexAction($msg, $code)
    {
        $result = ['msg' => $msg, 'statusCode' => $code];
        return $this->getResponse($result);
    }
}