<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-03
 * Time: 06:49
 */

namespace App\Api\Controllers;


use Core\ApiController;
use Infrastructure\Cache;
use Infrastructure\Places\GooglePlaces;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use ValueObjects\String\String;

/**
 * Class Places
 * @package App\Api\Controllers
 */
class Places extends ApiController
{
    /** @var $_model \App\Api\Models\Places */
    protected $_model;

    /**
     * @param $latitude
     * @param $longitude
     * @return Response
     */
    public function getAction($latitude, $longitude)
    {
        $results = $this->_model->getPlaces($latitude, $longitude);
        return $this->getResponse($results);
    }

    /**
     * @return Response
     */
    public function clearAction()
    {
        $results = $this->_model->clear();
        return $this->getResponse($results);
    }
}