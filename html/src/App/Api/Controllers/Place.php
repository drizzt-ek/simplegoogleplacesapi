<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-03
 * Time: 06:49
 */

namespace App\Api\Controllers;


use Core\ApiController;
use ValueObjects\String;

/**
 * Class Place
 * @package App\Api\Controllers
 */
class Place extends ApiController
{
    /** @var $_model \App\Api\Models\Place */
    protected $_model;

    /**
     * @param $idPlace
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAction($idPlace)
    {
        $results = $this->_model->getPlace($idPlace);

        return $this->getResponse($results);
    }
}