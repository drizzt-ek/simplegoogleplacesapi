<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-03
 * Time: 00:09
 */

namespace App\Front\Controllers;


use Core\FrontController;

/**
 * Class Home
 * @package App\Front\Controllers
 */
class Home extends FrontController
{
    /**
     * @return array
     */
    public function indexAction()
    {
        return array('title' => 'Simple application - Krzysztof Nowak');
    }
}