<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-03
 * Time: 01:15
 */

namespace Infrastructure\Places;

use Symfony\Component\HttpFoundation\Response;
use ValueObjects\Geography\Latitude;
use ValueObjects\Geography\Longitude;
use ValueObjects\String;

class GooglePlaces implements Places
{
    const API_URL = 'https://maps.googleapis.com/maps/api/place';
    const API_OUTPUT = 'json';//json|xml
    const API_SEARCHED_TYPE = 'bar';
    const API_DEFAULT_LANGUAGE = 'pl';

    const API_PARAM_KEY = 'key';
    const API_PARAM_LOCATION = 'location';
    const API_PARAM_RADIUS = 'radius';
    const API_PARAM_LANGUAGE = 'language';
    const API_PARAM_TYPE = 'type';
    const API_PARAM_PLACE_ID = 'placeid';

    const API_RESPONSE_STATUSES = array(
        'OK' => Response::HTTP_OK,
        'UNKNOWN_ERROR' => Response::HTTP_BAD_REQUEST,
        'ZERO_RESULTS' => Response::HTTP_NO_CONTENT,
        'OVER_QUERY_LIMIT' => Response::HTTP_BAD_REQUEST,
        'REQUEST_DENIED' => Response::HTTP_UNAUTHORIZED,
        'INVALID_REQUEST' => Response::HTTP_BAD_REQUEST,
        'NOT_FOUND' => Response::HTTP_NO_CONTENT
    );
    private $_apiKey;

    public function __construct(String\String $apiKey)
    {
        $this->_apiKey = $apiKey;
    }

    public function getBarsInRadiusFromLocation(Latitude $latitude, Longitude $longitude)
    {
        $params = implode('&', array(
            $this->_getLocationParameter($latitude, $longitude),
            $this->_getLanguageParameter(),
            $this->_getRadiusParameter(),
            $this->_getSearchedTypeParameter(),
            $this->_getApiKeyParameter()
        ));
        $url = sprintf("%s/radarsearch/%s?%s"
            , self::API_URL
            , self::API_OUTPUT
            , $params
        );

        $result = $this->_curlCall($url);
        $result = $this->_changeToJson($result);
        $result = $this->_setStatusCodeToResponse($result);
        return $result;
    }

    public function getDetailsAboutPlace(String\String $idPlace)
    {
        $params = implode('&', array(
            $this->_getPlaceIdParameter($idPlace),
            $this->_getLanguageParameter(),
            $this->_getApiKeyParameter()
        ));
        $url = sprintf("%s/details/%s?%s"
            , self::API_URL
            , self::API_OUTPUT
            , $params
        );

        $result = $this->_curlCall($url);
        $result = $this->_changeToJson($result);
        $result = $this->_setStatusCodeToResponse($result);
        return $result;
    }


    private function _curlCall($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 60000);
        $data = curl_exec($ch);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        curl_close($ch);
        return $data;
    }

    /**
     * @param String\String $idPlace
     * @return string
     */
    private function _getPlaceIdParameter(String\String $idPlace)
    {
        return sprintf('%s=%s', self::API_PARAM_PLACE_ID, $idPlace->toNative());
    }

    /**
     * @param Latitude $latitude
     * @param Longitude $longitude
     * @return string
     */
    private function _getLocationParameter(Latitude $latitude, Longitude $longitude)
    {
        return sprintf('%s=%s,%s', self::API_PARAM_LOCATION, (string)$latitude->toNative(), (string)$longitude->toNative());
    }

    /**
     * @return string
     */
    private function _getRadiusParameter()
    {
        return sprintf('%s=%s', self::API_PARAM_RADIUS, self::RADIUS);
    }

    /**
     * @return string
     */
    private function _getLanguageParameter()
    {
        return sprintf('%s=%s', self::API_PARAM_LANGUAGE, self::API_DEFAULT_LANGUAGE);
    }

    /**
     * @return string
     */
    private function _getSearchedTypeParameter()
    {
        return sprintf('%s=%s', self::API_PARAM_TYPE, self::API_SEARCHED_TYPE);
    }

    /**
     * @return string
     */
    private function _getApiKeyParameter()
    {
        return sprintf('%s=%s', self::API_PARAM_KEY, $this->_apiKey->toNative());
    }

    /**
     * @param $result
     * @return mixed
     */
    private function _changeToJson($result)
    {
        $result = json_decode($result, true);
        return $result;
    }

    /**
     * @param $result
     * @return mixed
     */
    private function _setStatusCodeToResponse($result)
    {
        $statuses = self::API_RESPONSE_STATUSES;
        $result['statusCode'] = $statuses[$result['status']];
        return $result;
    }
}