<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-03
 * Time: 06:36
 */

namespace Infrastructure\Places;


use ValueObjects\Geography\Latitude;
use ValueObjects\Geography\Longitude;
use ValueObjects\String;

interface Places
{
    const RADIUS = 2000;

    /**
     * @param Latitude $latitude
     * @param Longitude $longitude
     * @return mixed
     */
    public function getBarsInRadiusFromLocation(Latitude $latitude, Longitude $longitude);

    /**
     * @param String\String $idPlace
     * @return mixed
     */
    public function getDetailsAboutPlace(String\String $idPlace);
}