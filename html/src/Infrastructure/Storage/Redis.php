<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-03
 * Time: 20:14
 */

namespace Infrastructure\Storage;


use Predis\Client;
use ValueObjects\Number;
use ValueObjects\String;

/**
 * Class Redis
 * @package Infrastructure\Storage
 */
class Redis implements Storage
{
    /**
     * @var Client
     */
    private $_redisConnection;

    /**
     * Redis constructor.
     */
    public function __construct()
    {
        $this->_redisConnection = new Client();
    }

    /**
     * @param String\String $key
     * @param $value
     */
    public function set(String\String $key, $value)
    {
        $this->_redisConnection->set($key->toNative(), $value);
    }

    /**
     * @param String\String $key
     * @return string
     */
    public function get(String\String $key)
    {
        return $this->_redisConnection->get($key->toNative());
    }

    /**
     * @param String\String $key
     */
    public function del(String\String $key)
    {
        $this->_redisConnection->del($key->toNative());
    }

    /**
     * @param $pattern
     * @return array
     */
    public function keys($pattern)
    {
        return $this->_redisConnection->keys($pattern);
    }

    /**
     * @param String\String $key
     * @param Number\Integer $time
     */
    public function expired(String\String $key, Number\Integer $time)
    {
        $this->_redisConnection->expire($key->toNative(), $time->toNative());
    }

}