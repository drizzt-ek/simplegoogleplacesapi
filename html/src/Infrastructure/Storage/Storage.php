<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-03
 * Time: 20:14
 */

namespace Infrastructure\Storage;


use ValueObjects\Number;
use ValueObjects\String;

/**
 * Interface Storage
 * @package Infrastructure\Storage
 */
interface Storage
{
    /**
     * @param String\String $key
     * @param $value
     * @return mixed
     */
    public function set(String\String $key, $value);

    /**
     * @param String\String $key
     * @return mixed
     */
    public function get(String\String $key);

    /**
     * @param String\String $key
     * @return mixed
     */
    public function del(String\String $key);

    /**
     * @param String\String $key
     * @param Number\Integer $time
     * @return mixed
     */
    public function expired(String\String $key, Number\Integer $time);
}