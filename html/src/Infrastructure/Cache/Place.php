<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-03
 * Time: 11:48
 */

namespace Infrastructure\Cache;


use ValueObjects\String;

/**
 * Class Place
 * @package Infrastructure\Cache
 */
class Place implements CacheInterface
{
    /**
     *
     */
    const EXPIRED_TIME = 3600;
    /**
     * @var string
     */
    private static $key = 'Place';

    /**
     * @param String\String $idPlace
     * @return mixed
     */
    public static function get(String\String $idPlace)
    {
        return Cache::get(self::_getKey($idPlace));
    }

    /**
     * @param String\String $idPlace
     * @param $value
     * @param int $expired
     */
    public static function set(String\String $idPlace, $value, $expired = self::EXPIRED_TIME)
    {
        Cache::set(self::_getKey($idPlace), $value, $expired);
    }

    /**
     * @param String\String $idPlace
     * @return mixed
     */
    public static function del(String\String $idPlace)
    {
        return Cache::del(self::_getKey($idPlace));
    }

    /**
     * @param String\String $idPlace
     * @return String
     */
    private static function _getKey(String\String $idPlace)
    {
        return String\String::fromNative(sprintf("%s:%s", self::$key, $idPlace->toNative()));
    }
}