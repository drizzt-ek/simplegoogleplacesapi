<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-03
 * Time: 11:48
 */

namespace Infrastructure\Cache;


use ValueObjects\Geography\Latitude;
use ValueObjects\Geography\Longitude;
use ValueObjects\String;

class Places
{
    /**
     * @var string
     */
    private static $key = 'Places';

    /**
     * @param Latitude $latitude
     * @param Longitude $longitude
     * @return mixed
     */
    public static function get(Latitude $latitude, Longitude $longitude)
    {
        return Cache::get(self::_getKey($latitude, $longitude));
    }

    /**
     * @param Latitude $latitude
     * @param Longitude $longitude
     * @param $value
     */
    public static function set(Latitude $latitude, Longitude $longitude, $value)
    {
        Cache::set(self::_getKey($latitude, $longitude), $value);
    }

    /**
     * @param Latitude $latitude
     * @param Longitude $longitude
     */
    public static function del(Latitude $latitude, Longitude $longitude)
    {
        Cache::del(self::_getKey($latitude, $longitude));
    }

    /**
     * @param Latitude $latitude
     * @param Longitude $longitude
     * @return String\String
     */
    private static function _getKey(Latitude $latitude, Longitude $longitude)
    {
        return String\String::fromNative(sprintf("%s:%f:%f", self::$key, $latitude->toNative(), $longitude->toNative()));
    }
}