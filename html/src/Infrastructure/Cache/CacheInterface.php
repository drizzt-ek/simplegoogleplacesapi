<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-03
 * Time: 11:48
 */

namespace Infrastructure\Cache;


use ValueObjects\String;

/**
 * Interface CacheInterface
 * @package Infrastructure\Cache
 */
interface CacheInterface
{
    /**
     * @param String\String $key
     * @param $value
     * @param $expire
     * @return mixed
     */
    public static function set(String\String $key, $value, $expire);

    /**
     * @param String\String $key
     * @return mixed
     */
    public static function get(String\String $key);

    /**
     * @param String\String $key
     * @return mixed
     */
    public static function del(String\String $key);
}