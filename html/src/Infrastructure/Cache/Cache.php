<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-03
 * Time: 10:08
 */

namespace Infrastructure\Cache;


use Infrastructure\Storage\Storage;
use ValueObjects\Number\Integer;
use ValueObjects\String;

/**
 * Class Cache
 * @package Infrastructure\Cache
 */
class Cache implements CacheInterface
{
    /**
     * 1h default lifetime
     */
    const EXPIRED_TIME = 3600;
    /**
     * @var $_storageInstance
     */
    private static $_storageInstance;


    /**
     * @param Storage $storage
     */
    public static function init(Storage $storage)
    {
        self::$_storageInstance = $storage;
    }

    /**
     * @param String\String $key
     * @param $value
     * @param int $expire
     * @throws \Exception
     */
    public static function set(String\String $key, $value, $expire = self::EXPIRED_TIME)
    {
        self::_checkExistConnection();
        self::$_storageInstance->set($key, json_encode($value));
        self::$_storageInstance->expired($key, Integer::fromNative($expire));
    }

    /**
     * @param String\String $key
     * @return mixed
     * @throws \Exception
     */
    public static function get(String\String $key)
    {
        self::_checkExistConnection();
        return json_decode(self::$_storageInstance->get($key), true);
    }

    /**
     * @param String\String $key
     * @return mixed
     * @throws \Exception
     */
    public static function del(String\String $key)
    {
        self::_checkExistConnection();
        return json_decode(self::$_storageInstance->del($key), true);
    }

    /**
     * @throws \Exception
     */
    private static function _checkExistConnection()
    {
        if (empty(self::$_storageInstance)) {
            throw new \Exception('Storage instance not exist');
        }
    }
}