<?php
/**
 * Created by PhpStorm.
 * User: Krzysztof Nowak
 * Date: 2016-06-03
 * Time: 06:29
 */

namespace Core;

use Infrastructure\Places\Places;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiController
 * @package Core
 */
class ApiController
{
    /**
     * @var Request
     */
    protected $_request;
    /**
     * @var Response
     */
    protected $_response;
    /**
     * @var Model
     */
    protected $_model;

    /**
     * ApiController constructor.
     * @param Request $request
     * @param Response $response
     * @param Model $model
     */
    public function __construct(Request $request, Response $response, Model $model)
    {
        $this->_request = $request;
        $this->_response = $response;
        $this->_model = $model;
    }

    /**
     * @param $data
     * @param int $statusCode
     * @return Response
     */
    protected function getResponse($data, $statusCode = Response::HTTP_OK)
    {
        if (empty($data)) {
            $statusCode = Response::HTTP_NO_CONTENT;
        }
        if (!empty($data['statusCode'])) {
            $statusCode = $data['statusCode'];
        }
        $this->_response->setStatusCode($statusCode);

        if (method_exists($this->_response, 'setData')) {
            $this->_response->setData($data);
        } else {
            if (is_array($data)) {
                throw new \InvalidArgumentException('String expected in $data parameters');
            }
            $this->_response->setContent($data);
        }
        return $this->_response;
    }
}